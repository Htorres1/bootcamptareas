﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AceptadoTrigger : MonoBehaviour
{
    [SerializeField] GameObject trigger; //recibe el gameobject que se usa como trigger
    private bool _aceptado;
    public bool Aceptado
    {
        get => _aceptado;
        set
        {
            _aceptado = value;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Aceptado");
        Aceptado = true; //Asigna el valor de true a la variable aceptado

        if (other.gameObject != null)
        {
            Destroy(other.gameObject);
        }
    }
    
}

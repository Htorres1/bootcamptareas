﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OcultarCarpeta : MonoBehaviour
{
    [SerializeField] GameObject brazoCarpeta;
    [SerializeField] GameObject botonOcultar;
    [SerializeField] GameObject botonMostrar;

    public void MovimientoOcultarCarpeta() //Al activar este metodo se habilita el boton mostrar, se mueve el brazo y se deshabilita el boton ocultar
    {
        botonMostrar.SetActive(true);
        LeanTween.moveX(brazoCarpeta, 0.447f, 0.5f);
        botonOcultar.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoEnFrentePersona : MonoBehaviour
{
    private Vector3 _ultimaPosicionX; //Vectores que se usaran para almacenar las posiciones del mouse en el eje x
    private Vector3 _primeraPosicionX;
    private Vector3 _movimientoActual;

    GameObject personaEnFrente;
   
    [SerializeField]GameObject [] personas; //array que contendra a los personajes del nivel

    [SerializeField] float speed = 10f;



    void Update()
    {
        for (int i = 0; i < personas.Length; i++)
        {


            if (!personaEnFrente) // si no existe un valor en esa varible entonces se le asigna uno
            {
                personaEnFrente = personas[i];
                
            }

            else
            {
                personaEnFrente.transform.position = Vector3.MoveTowards(personaEnFrente.transform.position, new Vector3(0f, 0f, -7f), speed * Time.deltaTime); // los mueve a esa posicion
                personaEnFrente.GetComponent<Animator>().SetBool("Movimiento", true); //activa la animacion

                if (Input.GetMouseButtonDown(0))
                {

                    _primeraPosicionX = new Vector3(Input.mousePosition.x, 0f, 0f); //captura la primera posicion del mouse en el eje x al pulsar el boton izquierdo el mouse

                }

                if (Input.GetMouseButton(0))
                {
                    _ultimaPosicionX = new Vector3(Input.mousePosition.x, 0f, 0f); //captura la ultima posicion del mouse en el eje x al pulsar el boton izquierdo el mouse

                    _movimientoActual = new Vector3(_ultimaPosicionX.x - _primeraPosicionX.x, 0f, 0f); //se obtiene la distancia entre esos puntos


                }

                if (Input.GetMouseButtonUp(0))
                {

                    _ultimaPosicionX = _primeraPosicionX = Vector3.zero;
                    _movimientoActual = Vector3.zero;
                }



                if (_movimientoActual.x > 0) // al mover hacia la derecha el muñeco se mueva a esa direccion
                {
                        personaEnFrente.transform.position = Vector3.MoveTowards(personaEnFrente.transform.position, new Vector3(3f, 0f, -7f), 2f * Time.deltaTime);
                        personaEnFrente.GetComponent<Animator>().SetBool("Aceptado", true);
                }

                if (_movimientoActual.x < 0) // al mover hacia la izquierda el muñeco se mueva a esa direccion
                {
                        personaEnFrente.transform.position = Vector3.MoveTowards(personaEnFrente.transform.position, new Vector3(-3f, 0f, -7f), 2f * Time.deltaTime);

                        personaEnFrente.GetComponent<Animator>().SetBool("Rechazado", true);

                }



            }
        }

    }

}

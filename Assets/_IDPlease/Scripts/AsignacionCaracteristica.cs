﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsignacionCaracteristica : MonoBehaviour
{
    [SerializeField] string colorRopa; // se introduce el color de la ropa del personaje
    private CaracteristicaPersona _caracteristicaPersona;
    private void Awake()
    {
        _caracteristicaPersona = GameObject.Find("CaracteristicaPersona").GetComponent<CaracteristicaPersona>(); //se referencia al gameobject y se le asigna el color a la variable ColorRemera
        _caracteristicaPersona.ColorRemera = colorRopa;
    }
}

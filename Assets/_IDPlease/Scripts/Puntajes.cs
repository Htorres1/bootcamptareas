﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Puntajes : MonoBehaviour
{
    AceptadoTrigger aceptadoTrigger; // variables que haran referencia a los booleanos de Aceptado y Rechazado
    RechazadoTrigger rechazadoTrigger;

    [SerializeField] string colorNivel; //color que se usara para comparar con los de los personajes
    CaracteristicaPersona caracteristicaPersona;

    [SerializeField] TextMeshProUGUI ptCorrecto; //variables que sirven para asignar los elementos UI
    [SerializeField] TextMeshProUGUI ptIncorrecto;

    private int _contCorrecto=0; // contadores de puntos
    private int _contIncorrecto=0;

    private void Awake()
    {
        caracteristicaPersona = GameObject.Find("CaracteristicaPersona").GetComponent<CaracteristicaPersona>();

        aceptadoTrigger = GameObject.Find("AceptadoTrigger").GetComponent<AceptadoTrigger>();

        rechazadoTrigger = GameObject.Find("RechazadoTrigger").GetComponent<RechazadoTrigger>();
    }

    private void Update()
    {
        //Si Aceptado es true se comprueba si el color de la remera del personaje coincide con el del nivel, de acuerdo a eso se asignan los puntos
        if (aceptadoTrigger.Aceptado)
        {
            aceptadoTrigger.Aceptado = false;
           

            if (colorNivel == caracteristicaPersona.ColorRemera)
            {
                Debug.Log("Punto correcto");
                _contCorrecto++;
                ptCorrecto.text = "Correctos: " + _contCorrecto;
            }
            else
            {
                _contIncorrecto++;
                ptIncorrecto.text = "Incorrectos: " + _contIncorrecto;
            }
        }

        //Si Rechazado es true se comprueba si el color de la remera del personaje coincide con el del nivel, de acuerdo a eso se asignan los puntos
        if (rechazadoTrigger.Rechazado)
        {
            rechazadoTrigger.Rechazado = false;
            //Debug.Log("Punto incorrecto");

            if (colorNivel == caracteristicaPersona.ColorRemera)
            {
                _contIncorrecto++;
                ptIncorrecto.text = "Incorrectos: " + _contIncorrecto;
            }
            else
            {
                _contCorrecto++;
                ptCorrecto.text = "Correctos: " + _contCorrecto;
            }
            
        }


       
       
       
        

    }

     
}

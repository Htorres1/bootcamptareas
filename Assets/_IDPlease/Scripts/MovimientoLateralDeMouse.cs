﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoLateralDeMouse : MonoBehaviour
{
    [SerializeField] GameObject persona;
    private Vector3 _ultimaPosicionX;
    private Vector3 _primeraPosicionX;
    private Vector3 _movimientoActual;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {

            _primeraPosicionX = new Vector3(Input.mousePosition.x,0f,0f);
            
        }
        
        if (Input.GetMouseButtonUp(0))
        {
            _ultimaPosicionX = new Vector3(Input.mousePosition.x, 0f, 0f);

            _movimientoActual = new Vector3(_ultimaPosicionX.x - _primeraPosicionX.x,0f,0f) ;


        }

        if (_movimientoActual.x > 0) // al mover hacia la derecha el muñeco se mueva a esa direccion
        {
          persona.transform.Translate(-1f * Time.deltaTime,0f,0f);
            persona.GetComponent<Animator>().SetBool("Aceptado",true);
        }

        if (_movimientoActual.x < 0 ) // al mover hacia la izquierda el muñeco se mueva a esa direccion
        {
          persona.transform.Translate(1f* Time.deltaTime,0f,0f);
            persona.GetComponent<Animator>().SetBool("Rechazado", true);
        }

       
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MostrarCarpeta : MonoBehaviour
{
    [SerializeField] GameObject brazoCarpeta; //Se asignan los botones y el brazo que contiene la carpeta
    [SerializeField] GameObject botonOcultar;
    [SerializeField] GameObject botonMostrar;

    public void MovimientoCarpeta() //Al activar este metodo se habilita el boton ocultar, se mueve el brazo y se deshabilita el boton mostrar
    {
        botonOcultar.SetActive(true);
        LeanTween.moveX(brazoCarpeta, 0.802f, 0.5f);
        botonMostrar.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RechazadoTrigger : MonoBehaviour
{
    [SerializeField] GameObject trigger;
    private bool _rechazado;
    public bool Rechazado
    {
        get => _rechazado;
        set
        {
            _rechazado = value;
            
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Rechazado"); //Asigna el valor de true a la variable rechazado
        Rechazado = true;

        if (other.gameObject != null)
        {
            Destroy(other.gameObject);
        }

    }
}

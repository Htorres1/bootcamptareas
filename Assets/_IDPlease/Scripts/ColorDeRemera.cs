﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorDeRemera : MonoBehaviour
{
    [SerializeField] private string color;

    private string _colorRemera;

    private void Awake()
    {
        ColorRemera = color;
    }
    public string ColorRemera
    {
        get => _colorRemera;

        set { _colorRemera = value; }
    }
}

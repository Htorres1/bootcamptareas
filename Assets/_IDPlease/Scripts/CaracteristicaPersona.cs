﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaracteristicaPersona : MonoBehaviour
{

    private string _colorRemera;

    public string ColorRemera
    {
        get => _colorRemera;

        set { _colorRemera = value; }
    }
}

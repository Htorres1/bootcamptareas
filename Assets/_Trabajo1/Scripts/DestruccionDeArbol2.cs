﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestruccionDeArbol2 : MonoBehaviour
{

    SistemaPuntuacion sistemaPuntuacion;
    private ManagerJuego managerJuego;
    [SerializeField] private AudioClip audioClip;
    ControladorDeAudio controladorDeAudio;
    [SerializeField] private GameObject explosionParticulasPrefab;

    void Awake()
    {
        sistemaPuntuacion = GameObject.Find("SistemaPuntuacion").GetComponent<SistemaPuntuacion>();

        managerJuego = GameObject.Find("ManagerJuego").GetComponent<ManagerJuego>();

        controladorDeAudio = GameObject.Find("ControladorDeAudio").GetComponent<ControladorDeAudio>();
    }

    private void OnCollisionEnter(Collision collision)
    {

        Destroy(collision.gameObject);
        Destroy(gameObject);
        controladorDeAudio.ReproducirSonido(audioClip);
        if (explosionParticulasPrefab)
        {
            GameObject explosion = Instantiate(explosionParticulasPrefab, transform.position, explosionParticulasPrefab.transform.rotation);
            Destroy(explosion, explosion.GetComponent<ParticleSystem>().main.startLifetimeMultiplier);
        }
        sistemaPuntuacion.Puntuacion(); //se llama al metodo para añadir un punto al score
        managerJuego.TroncosEnLevel2--;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanzamientoDePelota2 : MonoBehaviour
{
    [SerializeField] private Transform lanzador2;
    [SerializeField] private GameObject prefabPelota2;
    [SerializeField] private float speed2 = 100f;
    [SerializeField] private AudioClip audioClip2;
    ControladorDeAudio controladorDeAudio2;

    private void Awake()
    {
        controladorDeAudio2 = GameObject.Find("ControladorDeAudio").GetComponent<ControladorDeAudio>();
    }

    public void LanzamientoPelota()
    {
        controladorDeAudio2.ReproducirSonido(audioClip2);
        GameObject pelota = GameObject.Instantiate(prefabPelota2, lanzador2.position, lanzador2.rotation);
        pelota.GetComponent<Rigidbody>().velocity = transform.forward * speed2 * Time.deltaTime;
        Destroy(pelota, 10f);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DestruccionArbol : MonoBehaviour
{
    private SistemaPuntuacion sistemaPuntuacion; //se hace referencia a la clase sistema puntuacion
    private ManagerJuego managerJuego;
    [SerializeField] private AudioClip audioClip;
    ControladorDeAudio controladorDeAudio;
    [SerializeField] private GameObject explosionParticulasPrefab; // recibe el prefab de la particula

    void Awake()
    {
        sistemaPuntuacion = GameObject.Find("SistemaPuntuacion").GetComponent<SistemaPuntuacion>();

        managerJuego = GameObject.Find("ManagerJuego").GetComponent<ManagerJuego>();

        controladorDeAudio = GameObject.Find("ControladorDeAudio").GetComponent<ControladorDeAudio>();
    }

    
    private void OnCollisionEnter(Collision collision)
    {
       
        Destroy(collision.gameObject); //destruye el tronco
        Destroy(gameObject); //destruye la pelota
        controladorDeAudio.ReproducirSonido(audioClip);
        if (explosionParticulasPrefab) // si exite el gameobject, se intancia y posteriormente de destruye
        {
            GameObject explosion = Instantiate(explosionParticulasPrefab, transform.position, explosionParticulasPrefab.transform.rotation);
            Destroy(explosion, explosion.GetComponent<ParticleSystem>().main.startLifetimeMultiplier);
        }
        sistemaPuntuacion.Puntuacion(); //se llama al metodo para añadir un punto al score
        managerJuego.TroncosEnLevel--; // se reduce en uno la cantidad de troncos
    }
}


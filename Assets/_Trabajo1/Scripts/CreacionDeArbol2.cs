﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreacionDeArbol2 : MonoBehaviour
{
    [SerializeField] private int arbolCantidad = 35;
    [SerializeField] private GameObject[] arboles;
    [SerializeField] private GameObject arbolPrefab;
    [SerializeField] private Transform posicionDeCreacion;
    private ManagerJuego managerJuego;
    private Vector3 posicionTemp;
    void Awake()
    {
        InicioAleatorio();


    }

    void InicioAleatorio()
    {
        managerJuego = GameObject.Find("ManagerJuego").GetComponent<ManagerJuego>();

        int numeroRandom = Random.Range(1, arbolCantidad + 1);

        arboles = new GameObject[numeroRandom];

        float cont = 7.27f;

        for (int i = 0; i < numeroRandom; i++)
        {
            managerJuego.TroncosEnLevel2++;
            posicionTemp = posicionDeCreacion.transform.position;
            posicionTemp.y = transform.position.y + cont;
            GameObject arbol = GameObject.Instantiate(arbolPrefab, posicionTemp, Quaternion.Euler(-90f, 0f, 0f));
            arbol.transform.parent = this.transform;

            cont += 7.27f;
        }
    }
}

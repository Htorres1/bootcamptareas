﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimietoDeEnemigo : MonoBehaviour
{
    [SerializeField] private GameObject[] puntosDeRecorrido; // recibe los gameobjects que tienen las posiciones por donde va a moverse el enemigo
    int posActual=0; //variable para contar dentro del array
    [SerializeField] private int speed = 100;
    float RadioDelPunto = 1; // radio de contacto para los puntos de recorrido

    private void Update()
    {
        //Si la distancia del punto actual con el siguiente el menor
        if (Vector3.Distance(puntosDeRecorrido[posActual].transform.position,transform.position)<RadioDelPunto)
        {
            //se aumenta el contador para referencia al siguiente punto
            posActual++;
            if (posActual >= puntosDeRecorrido.Length) //si la posicion es mayor o igual cantidad de posiciones restea a cero
            {
                posActual = 0;
            }
        }
        //mueve al enemigo a traves de los puntos desde su posicion actual a la siguiente
        transform.position = Vector3.MoveTowards(transform.position, puntosDeRecorrido[posActual].transform.position, Time.deltaTime * speed);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlladorMenuUI : MonoBehaviour
{
    [SerializeField] private AudioClip audioClip;
    ControladorDeAudio controladorDeAudio;
    [SerializeField] Animator transicion; // se agrega la animacion creada para la transicion
    [SerializeField] float tiempoTransicion=1f;
    void Awake()
    {
        controladorDeAudio = GameObject.Find("ControladorDeAudio").GetComponent<ControladorDeAudio>(); //busca el gameobject y su componente correspondiente
    }

    public void LLamadaAJuego()
    {
        
        StartCoroutine(CargarNivel(SceneManager.GetActiveScene().buildIndex + 1)); //inicia la corrutina que contiene la escena de transicion
        controladorDeAudio.ReproducirSonido(audioClip);

    }



    IEnumerator CargarNivel(int nivelIndex)
    {
        //Reproducir animacion

        transicion.SetTrigger("Start");

        //Esperar

        yield return new WaitForSeconds(tiempoTransicion);

        //Cargar escena

        SceneManager.LoadScene(nivelIndex); //se carga la escena
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SistemaPuntuacion : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI score;
    [SerializeField] private TextMeshProUGUI highScore; //para la pantalla de ganador
    [SerializeField] private TextMeshProUGUI highScore1; //para la pantalla de perdedor


    private int _punto;

    private void Start()
    {
        highScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString(); //muestra en pantalla el high score
        highScore1.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
    }

    public void Puntuacion()
    {
        _punto++; //suma los puntos

        score.text = "Score: " + _punto; //almacena los puntos

        if (_punto > PlayerPrefs.GetInt("HighScore", 0)) //si la puntuacion es mayor al high score guarda el puntaje
        {
            PlayerPrefs.SetInt("HighScore", _punto); 

            highScore.text = _punto.ToString();
            highScore1.text = _punto.ToString(); //se guarda tambien para la otra pantalla
        }
       
    }

    public void Reset() //resetea el highscore
    {
        PlayerPrefs.DeleteAll(); //metodo que borra TODOS los datos guardados
        highScore.text = "0";
        highScore1.text = "0";
    }
}

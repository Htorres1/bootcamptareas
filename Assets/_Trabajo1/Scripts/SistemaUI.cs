﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SistemaUI : MonoBehaviour
{
    [SerializeField] private GameObject pantallaGanador;
    [SerializeField] private GameObject pantallaPerdedor;
    [SerializeField] private AudioClip audioClip;
    ControladorDeAudio controladorDeAudio;

    private void Awake()
    {
        controladorDeAudio = GameObject.Find("ControladorDeAudio").GetComponent<ControladorDeAudio>(); //busca el gameobject y su componente correspondiente
    }

    public void PantallaGanador()
    {
        pantallaGanador.SetActive(true); //activa la imagen para mostrar en pantalla
    }

    public void PantallaPerdedor()
    {
        pantallaPerdedor.SetActive(true); //activa la imagen para mostrar en pantalla
    }

    public void LLamadaMenuPrincipal()
    {
        SceneManager.LoadScene("MainMenuTrabajo1"); //carga la escena
        controladorDeAudio.ReproducirSonido(audioClip);
    }

    public void LLamadaNuevoJuego()
    {
        SceneManager.LoadScene("Trabajo1"); //carga la escena
        controladorDeAudio.ReproducirSonido(audioClip);
    }
}

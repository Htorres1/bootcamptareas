﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ManagerJuego : MonoBehaviour
{
    private int _troncoEnLevel; //variables que sirve para almacenar la cantidad de troncos creados
    private int _troncoEnLevel2;
    [SerializeField] private SistemaUI sistemaUI;
    [SerializeField] private AudioClip audioGanador;
    [SerializeField] private CinemachineVirtualCamera[] camaras; // recibe a las camaras existentes
    [SerializeField] private GameObject[] botones; //se añade los botones que disparan, correspondiente a cada nivel
    ControladorDeAudio controladorDeAudio;

    private void Awake()
    {
        controladorDeAudio = GameObject.Find("ControladorDeAudio").GetComponent<ControladorDeAudio>();
    }

 
    public int TroncosEnLevel
    {
        get => _troncoEnLevel;

        set {
            _troncoEnLevel = value;

            if (_troncoEnLevel == 0) //si no hay troncos en el primer arbol
            {
                botones[0].SetActive(false);//se desactiva el boton del primer nivel
                camaras[0].Priority = 0; //se mueve la camara al segundo nivel
                botones[1].SetActive(true);//se activa el boton del segundo nivel
            }
        }
    }

    public int TroncosEnLevel2
    {
        get => _troncoEnLevel2;

        set
        {
            _troncoEnLevel2 = value;

            if (_troncoEnLevel2 == 0) //si no hay troncos en el segundo arbol llama a la imagen del ganador y reproduce un sonido de victoria
            {
                sistemaUI.PantallaGanador();
                controladorDeAudio.ReproducirSonido(audioGanador);
            }
        }
    }


}

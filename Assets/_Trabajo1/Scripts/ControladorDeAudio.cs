﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorDeAudio : MonoBehaviour
{
    [SerializeField] AudioSource audioSource; // recibe un componente audiosource
    private AudioClip _clip; // recibe un audio

    public void ReproducirSonido(AudioClip clip)
    {
        _clip = clip;
        audioSource.PlayOneShot(_clip); //reproduce el audio
    }
}

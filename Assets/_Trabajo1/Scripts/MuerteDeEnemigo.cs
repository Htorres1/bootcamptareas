﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuerteDeEnemigo : MonoBehaviour
{
    [SerializeField]private SistemaUI sistemaUI;
    [SerializeField] private AudioClip audioClip;
    [SerializeField] private AudioClip audioPerdedor;
    ControladorDeAudio controladorDeAudio;
    [SerializeField] private GameObject explosionParticulasPrefab; //recibe el prefab de la particula del enemigo

    private void Awake()
    {
        controladorDeAudio = GameObject.Find("ControladorDeAudio").GetComponent<ControladorDeAudio>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        controladorDeAudio.ReproducirSonido(audioClip);
            sistemaUI.PantallaPerdedor(); //llama a la imagen del perdedor a traves del metodo del sistemaui
        controladorDeAudio.ReproducirSonido(audioPerdedor);
        if (explosionParticulasPrefab) // si exite el gameobject, se intancia y posteriormente de destruye
        {
            GameObject explosion = Instantiate(explosionParticulasPrefab, transform.position, explosionParticulasPrefab.transform.rotation);
            Destroy(explosion, explosion.GetComponent<ParticleSystem>().main.startLifetimeMultiplier);
        }
    }
}

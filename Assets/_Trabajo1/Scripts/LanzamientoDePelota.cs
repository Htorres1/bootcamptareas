﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanzamientoDePelota : MonoBehaviour
{
    [SerializeField] private Transform lanzador; //recibe el transform del gameobject padre que va contener a la pelota
    [SerializeField] private GameObject prefabPelota;
    [SerializeField] private float speed=100f; //velocidad de lanzamiento
    [SerializeField] private AudioClip audioClip;
    ControladorDeAudio controladorDeAudio;

    private void Awake()
    {
        controladorDeAudio = GameObject.Find("ControladorDeAudio").GetComponent<ControladorDeAudio>();
    }

    public void LanzamientoPelota()
    {
        controladorDeAudio.ReproducirSonido(audioClip);
        GameObject pelota = GameObject.Instantiate(prefabPelota,lanzador.position,lanzador.rotation); //instancia el prefab de la pelota en la posicion del padre
        pelota.GetComponent<Rigidbody>().velocity=transform.forward * speed * Time.deltaTime; //agrega una velocidad a la pelota para que se mueva
        Destroy(pelota,10f); //destruye la pelota en diez segundo si no colisiona
    }
}

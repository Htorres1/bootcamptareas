﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoDeAbajoHaciaArriba : MonoBehaviour
{
    [SerializeField] private GameObject arbol; //recibe el gameobject que contiene a los troncos

    void Start()
    {
        LeanTween.moveY(arbol,7.27f,1f); //se mueve al padre desde su posicion actual en el ejeY a 7.27 en 1 segundo
    }

    
}

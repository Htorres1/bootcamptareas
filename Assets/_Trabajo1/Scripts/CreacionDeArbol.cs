﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreacionDeArbol : MonoBehaviour
{
    [SerializeField] private int arbolCantidad = 35; //cantidad a crear
    [SerializeField] private GameObject [] arboles;
    [SerializeField] private GameObject arbolPrefab; // recibe el prefab del tronco
    [SerializeField] private Transform posicionDeCreacion; //recibe el transform del gameobject padre
    private ManagerJuego managerJuego; //variable de tipo managerjuego referencia a la clase del mismo nombre
    private Vector3 posicionTemp; //variable que guarda una poscion temporalmente
    void Awake()
    {
        InicioAleatorio();
        
    }

    void InicioAleatorio()
    {
        managerJuego = GameObject.Find("ManagerJuego").GetComponent<ManagerJuego>(); //busca el gameobject y su componente correspondiente

        int numeroRandom = Random.Range(1, arbolCantidad+1); // se crea un integer que hace que se cree alturas aleatorias usando el Random.Range()

        arboles = new GameObject[numeroRandom]; //se guarda el valor aleatorio dentro del array

        float cont = 7.27f; // variable que se para ubicar al tronco en el eje Y

        for (int i =0; i<numeroRandom;i++)
        {
            managerJuego.TroncosEnLevel++; //cuenta la cantidad de tronco creada
            posicionTemp = posicionDeCreacion.transform.position;//se guarda la posicion del padre
            posicionTemp.y = transform.position.y + cont; // se suma en el eje Y para ubicar al tronco que sigue en esa posicion
            GameObject arbol = GameObject.Instantiate(arbolPrefab,posicionTemp, Quaternion.Euler(-90f, 0f, 0f)); //se instancia el prefab del tronco en la posicion del padre
            arbol.transform.parent = this.transform; //se muestra el prefab dentro el padre

            cont+=7.27f; //se aumenta el valor sumandole el mismo valor
        }
    }

    

}

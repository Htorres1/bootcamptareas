﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeVida : MonoBehaviour
{
    public Slider slider;

    public void AsignarSaludMaxima(int salud) //se le asigna el calor maximo de vida y el valor actual de la vida
    {
        slider.maxValue = salud;
        slider.value = salud;
    }

    public void AsignarSalud(int salud)
    {
        slider.value = salud;
    }
}

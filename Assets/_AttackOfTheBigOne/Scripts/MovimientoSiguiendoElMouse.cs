﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoSiguiendoElMouse : MonoBehaviour
{
    [SerializeField] Transform aSeguir;
    private float _limiteMovimiento = 15f; //limite al cual se va acercar el player a la mira de disparo
    [SerializeField] float speed = 10f;
    private Animator animator;
    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(aSeguir); //mira al objectivo

        //_direccionMovimiento = (aSeguir.position - transform.position).normalized;

        if ((transform.position-aSeguir.position).magnitude >= _limiteMovimiento) //si la distancia es mayor al limite entonces se mueva hacia el
        {
            transform.Translate(0f,0f, speed * Time.deltaTime);
            animator.SetBool("Movimiento", true);
        }
        else
        {
            speed = 0f;
            animator.SetBool("Movimiento", false);
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DañoAPlayer : MonoBehaviour
{
    PlayerVida playerVida;
    
    int daño = 5;

    private void Awake()
    {
        playerVida = GetComponent<PlayerVida>();
    }
    private void OnTriggerEnter(Collider other) //llama al metedo que actualiza la vida del player
    {
        playerVida.RecibeDaño(daño);
    }
}

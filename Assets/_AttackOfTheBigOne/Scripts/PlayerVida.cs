﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVida : MonoBehaviour
{
    private int _salud = 100;
    BarraDeVida barraDeVida;
    AdministradorUI administradorUI;

    [SerializeField] GameObject enemigos;

    public int Salud
    {
        get => _salud;
        set
        {
            _salud = value;
        }
    }

    private void Awake()
    {
        barraDeVida = GameObject.Find("BarraDeSaludPlayer").GetComponent<BarraDeVida>();
        barraDeVida.AsignarSaludMaxima(Salud);

        administradorUI = GameObject.Find("AdministradorUI").GetComponent<AdministradorUI>();

    }

    private void Update()
    {
    }
    public void RecibeDaño(int daño)
    {
        Salud -= daño; //decrementa el valor de la vida

        barraDeVida.AsignarSalud(Salud);

        if (Salud <= 0) //si llega a 0 destruye el gameobject, activa la pantalla de gameover y desactiva a los enemigos
        {
            Destroy(gameObject);
            administradorUI.ActivarPantallaGameOver();

            enemigos.SetActive(false);
        }
    }
}

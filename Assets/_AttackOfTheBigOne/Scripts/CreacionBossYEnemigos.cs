﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreacionBossYEnemigos : MonoBehaviour
{
    [SerializeField] private Transform posicionBoss;
    [SerializeField] private GameObject prefabBoss;
    [SerializeField] private GameObject prefabEnemigo;
    private GameObject[] _enemigos;

    private void Awake()
    {
        CreacionEnemigos();
    }
    // Update is called once per frame
    void CreacionEnemigos ()
    {
        //Se crea el boss
        GameObject boss = Instantiate(prefabBoss, posicionBoss.position, Quaternion.identity);
        boss.transform.parent=this.transform;
        //Cantidad aleatoria de enemigo
         int cant = Random.Range(4, 11);

        _enemigos = new GameObject[cant];

        for (int i = 0; i <= cant; i++)
        {
                GameObject enemigo = Instantiate(prefabEnemigo, posicionBoss.position, Quaternion.identity);
                enemigo.transform.parent = this.transform;
        }
    }
}

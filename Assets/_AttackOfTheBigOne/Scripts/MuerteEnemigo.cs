﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuerteEnemigo : MonoBehaviour
{
    private bool _detener = false;

    private void Update()
    {
       
        if (_detener)
        {
            DetenerMovimiento();
            _detener = false;
        }
    }

    public void DetenerMovimiento() //se destruye despues de 3 segundos
    {
        _detener = true;
        Destroy(gameObject,3f);
       
    }
}

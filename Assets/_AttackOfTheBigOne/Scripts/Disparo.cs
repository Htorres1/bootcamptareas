﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    LineRenderer lr;
    bool puedeDisparar;
    int daño = 30;
    [SerializeField] Transform posicionDisparo;
    [SerializeField] Transform aSeguir;
    void Awake()
    {
        lr = GetComponent<LineRenderer>();
        lr.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            puedeDisparar = true;
            DispararLaser(transform.forward*300f);
        }
        else if(Input.GetMouseButtonUp(0))
        {
            lr.enabled = false;
            puedeDisparar = true;
        }
       
    }

    public void DispararLaser(Vector3 posicionObjetivo)
    {
       
        if (puedeDisparar)
        {
            RaycastHit hit;
            if(Physics.Raycast(posicionDisparo.position,aSeguir.TransformDirection(Vector3.up), out hit,Mathf.Infinity)) //crea una linea desde la posicion de la pistola hasta el infinito con direccion de la mira de disparo
            {
                //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                Debug.Log("Disparo");

                Boss boss = hit.transform.GetComponent<Boss>();
                if (boss!=null)
                {
                    boss.RecibeDaño(daño);
                }

                MuerteEnemigo enemigo = hit.transform.GetComponent<MuerteEnemigo>();
                if (enemigo!=null)
                {
                    enemigo.DetenerMovimiento();
                }
            }
            lr.SetPosition(0, posicionDisparo.position);
            lr.SetPosition(1, posicionObjetivo);
            lr.enabled = true;
            puedeDisparar = false;
        }
    }
}

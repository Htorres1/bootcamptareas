﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Boss : MonoBehaviour
{
    private int _salud = 100;
    BarraDeVida barraDeVida;
    AdministradorUI administradorUI;

    public int Salud
    {
        get => _salud;
        set
        {
            _salud = value;
        }
    }

    private void Awake()
    {
        barraDeVida = GameObject.Find("BarraDeSalud").GetComponent<BarraDeVida>();
        barraDeVida.AsignarSaludMaxima(Salud);
        administradorUI = GameObject.Find("AdministradorUI").GetComponent<AdministradorUI>();
    }

    private void Update()
    {
    }
    public void RecibeDaño(int daño)
    {
        Salud -= daño;
        barraDeVida.AsignarSalud(Salud);

        if (Salud<=0) //si llega a 0 la vida del boss se destruye el gameobject y llama a la pantalla de level completed
        {
            Destroy(gameObject);
            administradorUI.ActivarPantallaLevelCompleted();
        }
    }
}

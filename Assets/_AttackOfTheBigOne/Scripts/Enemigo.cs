﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemigo : MonoBehaviour
{
    NavMeshAgent pathFinder;
    Transform target;
    private Animator animator;

    void Awake()
    {
        pathFinder = GetComponent<NavMeshAgent>();
        target = GameObject.FindGameObjectWithTag("Player").transform; //se asigna al gameobject que va seguir por medio del tag
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        pathFinder.SetDestination(target.position); // sigue al objectivo
        animator.SetBool("Movimiento", true);

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AdministradorUI : MonoBehaviour
{
    [SerializeField] GameObject pantallaGameOver;
    [SerializeField] GameObject pantallaLevelCompleted;

    //Activa las pantallas cuando los metodos son llamados

    public void ActivarPantallaGameOver()
    {
        pantallaGameOver.SetActive(true);

    }

    public void ActivarPantallaLevelCompleted()
    {

        pantallaLevelCompleted.SetActive(true);
    }

    //carga de nuevo la escena

    public void ResetearJuego()
    {
        SceneManager.LoadScene("AttackOfTheBigOnes");
    }
}

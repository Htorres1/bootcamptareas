﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CursorUI : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    [SerializeField] private RectTransform crossHair;
    private Vector3 _posStart;
    private Vector2 _canvasSize;
    [SerializeField] private Vector2 deltaValue;

    private Image _image;

    private void Awake()
    {
        _image = GetComponent<Image>();
    }

    public void EnableController()
    {
        _image.raycastTarget = true;
        crossHair.gameObject.SetActive(true);
    }

    // Use this for initialization
    void Start()
    {

        CursorMode cursorMode = CursorMode.ForceSoftware;
        Cursor.SetCursor(null, Vector2.zero, cursorMode);
        //Cursor.visible = false;
        _canvasSize.x = gameObject.GetComponent<RectTransform>().rect.width;
        _canvasSize.y = gameObject.GetComponent<RectTransform>().rect.height;
    }

    // Update is called once per frame
    void Update()
    {
        //Cursor.visible = false;
        //transform.position = Input.mousePosition;
        //Debug.Log(crossHair.transform.position +" == "+Input.mousePosition);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 crossPos = crossHair.anchoredPosition;
        crossPos += eventData.delta;
        if (crossPos.x > _canvasSize.x / 2)
        {
            crossPos.x = _canvasSize.x / 2;
        }
        if (crossPos.x < -_canvasSize.x / 2)
        {
            crossPos.x = -_canvasSize.x / 2;
        }
        if (crossPos.y > _canvasSize.y / 2)
        {
            crossPos.y = _canvasSize.y / 2;
        }
        if (crossPos.y < -_canvasSize.y / 2)
        {
            crossPos.y = -_canvasSize.y / 2;
        }
        crossHair.anchoredPosition = crossPos;
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        
    }

    public void OnPointerDown(PointerEventData eventData)
    {
       
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Puntaje : MonoBehaviour
{
    public static Puntaje instancia;

    [SerializeField] TextMeshProUGUI puntajeDisplay;

    private int _punto;

    private void OnEnable()
    {
        if (instancia==null)
        {
            instancia = this; //activa el script
        }
    }

    public void ActualizarPuntaje(int valor)
    {
        _punto += valor;

        puntajeDisplay.text = _punto.ToString();
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StackearBloques : MonoBehaviour
{
    Transform bloquePadre;
    [SerializeField] Transform posicionStack;
    PlayerColor playerColor;

    bool enFinal;
    public bool lineaFinal;
    
    [SerializeField] float fuerzaForward;
    [SerializeField] float fuerzaAgregada;
    [SerializeField] float fuerzaReducida;

    public static Action<float> empuje;

    private void Awake()
    {
        playerColor = GameObject.Find("Player").GetComponentInChildren<PlayerColor>();    //Referencia al color del player
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("LineaFinalInicio"))
        {
            enFinal = true;
        }
        if (other.CompareTag("LineaFinal"))
        {
            lineaFinal = true;
        }

        if(enFinal)
        {
            return;
        }
        if (other.CompareTag("Bloque"))
        {
            Transform otherTransform = other.transform; //se guarda el transform del bloque

            if (playerColor.miColor==otherTransform.GetComponent<Bloque>().ObtenerColor()) //se compara los colores
            {
                Puntaje.instancia.ActualizarPuntaje(otherTransform.GetComponent<Bloque>().valor); //busca la variable "valor" en el bloque y asi obtener su valor asignado y suma al puntaje
            }
            else
            {
                Puntaje.instancia.ActualizarPuntaje(otherTransform.GetComponent<Bloque>().valor*-1); //resta en -1 el puntaje
                Destroy(other.gameObject); //destruyer el bloque que no corresponde con el color

                if (bloquePadre == null)
                {
                    if (bloquePadre.childCount>1)
                    {
                        bloquePadre.position -= Vector3.up * bloquePadre.GetChild(bloquePadre.childCount - 1).localScale.y;
                        Destroy(bloquePadre.GetChild(bloquePadre.childCount - 1).gameObject);
                    }
                    else
                    {
                        Destroy(bloquePadre.gameObject);
                    }
                }
                return;
            }
            

            Rigidbody otherRB = otherTransform.GetComponent<Rigidbody>(); //se obtiene el rb del bloque

            otherRB.isKinematic = true;

            other.enabled = false; //deshabilita el colider del bloque

            if (bloquePadre==null)
            {
                bloquePadre = otherTransform;
                bloquePadre.position = posicionStack.position;
                bloquePadre.parent = posicionStack;
            }
            else
            {
                bloquePadre.position += Vector3.up * (otherTransform.localScale.y);
                otherTransform.position = posicionStack.position;
                otherTransform.parent = posicionStack;
            }
        }
    }
}

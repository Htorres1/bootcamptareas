﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwerveInputSystem : MonoBehaviour
{
    //Este script procesa el input del user 

    private float _lastFingerPositionX; //ultimo frame de la posicion del dedo 
    private float _moveFactorX; //sigue la trayectoria del dedo desde el ultimo frame

    public float MoveFactorX => _moveFactorX;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _lastFingerPositionX = Input.mousePosition.x;

        }else if (Input.GetMouseButton(0))
        {
            _moveFactorX = Input.mousePosition.x - _lastFingerPositionX;
            _lastFingerPositionX = Input.mousePosition.x;

        }else if (Input.GetMouseButtonUp(0))
        {
            _moveFactorX = 0f;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerColor : MonoBehaviour
{
    [SerializeField] public Color miColor;
    [SerializeField] Renderer[] misRends; // array que sirve para almacenar los renders que van a cambiar de color

    Rigidbody rb;

    void Start()
    {
        AsignarColor(miColor);
        rb = GetComponent<Rigidbody>();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("MurallaDeColor"))
        {
            AsignarColor(other.GetComponent<MurallaDeColor>().ObtenerColor()); //se cambia el color del player al color de la muralla
        }   
    }

    void AsignarColor(Color color)
    {
        miColor = color;

        for (int i=0; i<misRends.Length;i++)
        {
            misRends[i].material.SetColor("_Color",miColor); //asigna los colores a los renders
        }
    }
}

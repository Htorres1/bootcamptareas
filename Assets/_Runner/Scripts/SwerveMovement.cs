﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwerveMovement : MonoBehaviour
{
    private SwerveInputSystem _swerveInput;
    [SerializeField] private float swerveSpeed=0.5f; // velocidad del esquive
    [SerializeField] private float maxSwerveAmount = 1f; //maxima cantidad de swerve
    private void Awake()
    {
        _swerveInput = GetComponent<SwerveInputSystem>();
    }

    private void Update()
    {
        float swerveAmount = _swerveInput.MoveFactorX * Time.deltaTime * swerveSpeed; //recibe la cantidad del movimiento
        swerveAmount = Mathf.Clamp(swerveAmount,-maxSwerveAmount,maxSwerveAmount);
        transform.Translate(swerveAmount,0,0);//mueve en el ejeX
    }
}

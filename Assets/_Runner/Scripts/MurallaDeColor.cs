﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MurallaDeColor : MonoBehaviour
{
    [SerializeField] Color nuevoColor;

    void Awake()
    {
        Color tempColor = nuevoColor;
        tempColor.a = 0.5f; // se cambia el alfa del color
        Renderer rend = transform.GetComponent<Renderer>();
        rend.material.SetColor("_Color", tempColor); //asigna el color a la muralla
    }

    // Update is called once per frame
    public Color ObtenerColor()
    {
        return nuevoColor;
    }
}

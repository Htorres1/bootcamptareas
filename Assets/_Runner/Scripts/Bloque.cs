﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bloque : MonoBehaviour
{
    private Renderer _rend;
    [SerializeField] public int valor;
    [SerializeField] Color color;

    void Start()
    {
        _rend = GetComponent<Renderer>();
        _rend.material.SetColor("_Color", color);
    }

    public Color ObtenerColor()
    {
        return color; //retorna el color
    }
}

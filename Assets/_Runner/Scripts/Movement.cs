﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] Rigidbody rb;
    [SerializeField] float speed = 200;
    StackearBloques stackearBloques;
    Animator animator;

    private void Awake()
    {
        rb =GameObject.Find("Runner").GetComponent<Rigidbody>();
        animator= GameObject.Find("Runner").GetComponent<Animator>();
        stackearBloques= GameObject.Find("Runner").GetComponent<StackearBloques>();
    }

    private void Update()
    {
        
            if (Input.GetMouseButton(0))
            {
                rb.velocity = transform.forward * speed * Time.deltaTime;
                animator.SetBool("Movimiento", true);
            }

             if (stackearBloques.lineaFinal == true)
            {
                rb.velocity = Vector3.zero;
            }
        
    }

}

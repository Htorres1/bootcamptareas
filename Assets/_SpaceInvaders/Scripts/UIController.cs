﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] GameObject gameOverScreen;
    [SerializeField] GameObject winScreen;
    [SerializeField] AudioController audioController;
    [SerializeField] AudioClip buttonPressfx;

    public void ActivateGameOverScreen()
    {
        gameOverScreen.SetActive(true);
        
    }

    public void ActivateWinScreen()
    {
       
        winScreen.SetActive(true);
    }

    public void MainMenu()
    {
        audioController.PlaySfx(buttonPressfx);
        SceneManager.LoadScene("MainMenu");
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeath : MonoBehaviour
{
   [SerializeField] private GameObject explosionParticlesPrefab;
    [SerializeField] private AudioClip explosion;
    AudioController audioController;
    private GameManager gameManager;


    private void Awake()
    {
        audioController = FindObjectOfType<AudioController>();
        gameManager = FindObjectOfType<GameManager>();
        if (gameManager != null)
        {
            gameManager.EnemyOnLevel++;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        gameManager.EnemyOnLevel--;
        ScoreSystem.scoreValue += 1;
        audioController.PlaySfx(explosion);

        if (explosionParticlesPrefab)
        {
            GameObject explosion = (GameObject)Instantiate(explosionParticlesPrefab, transform.position, explosionParticlesPrefab.transform.rotation);
            Destroy(explosion, explosion.GetComponent<ParticleSystem>().main.startLifetimeMultiplier);
        }

        Destroy(collision.gameObject);
        Destroy(gameObject);
        
    }

}

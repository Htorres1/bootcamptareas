﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private int _enemyOnLevel;
    [SerializeField] UIController uiController;

    public int EnemyOnLevel
    {
        get => _enemyOnLevel;

        set
        {
            _enemyOnLevel = value;

            if (_enemyOnLevel==0)
            {
                uiController.ActivateWinScreen();
            }
        }
    }
}

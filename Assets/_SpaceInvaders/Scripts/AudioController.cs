﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    [SerializeField]private AudioSource audioSource;
    [SerializeField] private AudioSource musicSource;

    private void Start()
    {
        musicSource.Play();
    }

    public void PlaySfx(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementEnemy : MonoBehaviour
{
    [SerializeField] float speed = 6f;
    private bool horizontal = false;
    private Vector3 currentPosition;

    void Update()
    {
        

        if (!horizontal)
        {
            

            if (transform.position.x <=65)
            {
                currentPosition = transform.position;
                currentPosition.x += 1 * speed * Time.deltaTime;
                transform.position = currentPosition;

                
               
            }
            else
            {
                transform.position += new Vector3(0f, -3f, 0f);
                horizontal = true;

            }
        }
        else
        {
            if (transform.position.x >= -68.1)
            {
                currentPosition = transform.position;
                currentPosition.x -= 1 * speed * Time.deltaTime;
                transform.position = currentPosition;
            }
            else
            {
                transform.position += new Vector3(0f, -3f, 0f);
                horizontal = false;
                
            }



        }

        //Vector3 currentPositionY = transform.position;
        //currentPositionY.y -= 1 * speedV * Time.deltaTime;
        //transform.position = currentPositionY;

    }
}

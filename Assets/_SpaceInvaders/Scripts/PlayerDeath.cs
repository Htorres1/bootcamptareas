﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    private UIController uiController;

    private void OnTriggerEnter(Collider other)
    {
        uiController = GameObject.Find("UIController").GetComponent<UIController>();
        uiController.ActivateGameOverScreen();
        Destroy(gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUIController : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip buttonPressedFx;
    [SerializeField] private GameObject startButton;


    public void PlaySfx()
    {
        audioSource.PlayOneShot(buttonPressedFx);
        
    }

    public void StartGame()
    {
        SceneManager.LoadScene("BootCampRoshka");
        ScoreSystem.scoreValue = 0;
    }
}

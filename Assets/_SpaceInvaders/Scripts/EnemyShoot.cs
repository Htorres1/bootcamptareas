﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    [SerializeField]private Transform launchPosition;
    [SerializeField]private GameObject bulletPrefab;
    [SerializeField]private float speed = 1000f;
    [SerializeField] private List<GameObject> enemyList = new List<GameObject>();

    private float nextShoot;
    private float delay = 5f;

    //private void Awake()
    //{
    //    for (int i = 0; i < transform.childCount; i++)
    //    {
    //        Transform tmpTransform = transform.GetChild(i);
    //        GameObject enemy = tmpTransform.GetComponent<GameObject>();
    //        if (enemy != null)
    //        {
    //            enemyList.Add(enemy);
    //        }
    //    }
    //}
    void Update()
    {
            if (Time.time > nextShoot && enemyList[Random.Range(0, enemyList.Count)])
            {
                nextShoot = Time.time + delay;
                Shoot();
                
            }   
    }

    private void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab, launchPosition.position, launchPosition.rotation);
        bullet.GetComponent<Rigidbody>().velocity = new Vector3(0f, -1f, 0f) * speed * Time.deltaTime;
        Destroy(bullet,20f);
        
    }
}

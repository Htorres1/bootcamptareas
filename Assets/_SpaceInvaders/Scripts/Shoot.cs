﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shoot : MonoBehaviour
{
    [SerializeField] private Transform launchPosition;
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private float speed = 100f;

    //Variables para tener un delay de 1 segundo en el disparo
    [SerializeField] private float nextShoot;
    [SerializeField] private float delay = 1f;

    AudioController audioController;
    [SerializeField] private AudioClip shoot;

    //Variables para el slider
    [SerializeField] private Slider slider;
    [SerializeField] private float timeRemining;
    [SerializeField] private float timeMax= 1f;


    private void Start()
    {
        audioController = FindObjectOfType<AudioController>();
    }

    void Update()
    {
        
        slider.value = CalculateSliderValue();

        if (Input.GetButtonDown("Fire1") && Time.time > nextShoot)
        {
            nextShoot = Time.time + delay;
            timeRemining = timeMax;
            BulletFire();
        }

        if (timeRemining<=0)
        {
            timeRemining = 0;

        }else if (timeRemining>0)
        {
            timeRemining -= Time.deltaTime; //Decrementa el valor usando el tiempo
        }
    }

    void BulletFire()
    {
        GameObject bullet = Instantiate(bulletPrefab, launchPosition.position,launchPosition.rotation);
        bullet.GetComponent<Rigidbody>().velocity = transform.up * speed *Time.deltaTime;
        audioController.PlaySfx(shoot);
        Destroy(bullet, 5f);
    }

    float CalculateSliderValue()
    {
        return (timeRemining / timeMax);
    }
}

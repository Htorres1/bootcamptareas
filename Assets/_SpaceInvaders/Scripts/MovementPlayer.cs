﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlayer : MonoBehaviour
{
    [SerializeField]float speed = 10f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A)&& transform.position.x>-68.7)
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.D)&& transform.position.x < 65)
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }

        //if (Input.GetKey(KeyCode.W) && transform.position.y < 36)
        //{
        //    transform.position += Vector3.up * speed * Time.deltaTime;
        //}

        //if (Input.GetKey(KeyCode.S) && transform.position.y > -37)
        //{
        //    transform.position += Vector3.down * speed * Time.deltaTime;
        //}
    }
}

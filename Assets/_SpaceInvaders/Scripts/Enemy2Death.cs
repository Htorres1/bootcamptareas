﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2Death : MonoBehaviour
{
    [SerializeField]private GameObject explosionParticlesPrefab;
    private int count = 0;
    [SerializeField] private AudioClip audioClip;
    private AudioController audioController;
    private GameManager gameManager;

    private void Start()
    {
        audioController = FindObjectOfType<AudioController>();
        gameManager = FindObjectOfType<GameManager>();
        if (gameManager != null)
        {
            gameManager.EnemyOnLevel++;
        }
    }
    void Update()
    {
        if (count==3)
        {
            gameManager.EnemyOnLevel--;
            ScoreSystem.scoreValue += 2;
            audioController.PlaySfx(audioClip);
            GameObject explosion = (GameObject)Instantiate(explosionParticlesPrefab, transform.position, explosionParticlesPrefab.transform.rotation);
            Destroy(explosion, explosion.GetComponent<ParticleSystem>().main.startLifetimeMultiplier);
            Destroy(gameObject);
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        count++;
        transform.localScale += new Vector3(-1f, -1f, -1f);
        Destroy(collision.gameObject);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tween : MonoBehaviour
{
    [SerializeField] private GameObject[] cubo;
    float speed=2f;
    // Start is called before the first frame update
    void Start()
    {
        LeanTween.moveY(cubo[0], -3, speed).setEaseInOutBounce();
        LeanTween.moveY(cubo[1], -3, speed).setEaseOutElastic();
        LeanTween.moveY(cubo[2], -3, speed).setEaseOutQuint().setLoopPingPong();

        LeanTween.moveY(cubo[3], -3, speed).setEaseOutBack().setLoopPingPong();
        LeanTween.rotateY(cubo[3], 90, 2).setEaseOutExpo().setLoopPingPong();
        LeanTween.moveY(cubo[4], -3, speed).setEaseInOutCirc();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            LeanTween.cancel(cubo[0]);
            transform.localScale = Vector3.one*2;
            LeanTween.scale(cubo[0], Vector3.one * 3,1.5f).setEasePunch();
            
        }

        
    }
        
}
